> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Rafael Rodriguez

### Assignment #2 Requirements:

*Sub-Heading:*

1. Install MySQL Workbench
2. Write a "Hello-world" Java Servlet
3. Write a Database Servlet Deploying Servlet using @WebServlet

#### README.md file should include the following items:

* Bullet-list items
* http://localhost:9999/hello   (Displays directory! It should not! Needs index.html)
* http://localhost:9999/hello/sayhello  (invokes HelloServlet)
    Note: /sayhello maps to HelloServlet.class(changed web.xml file)
* http://localhost:9999/hello/querybook.html 

> This is a blockquote.
> 
#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello/index.html*:

![http://localhost:9999/hello](img/hello.png)

*Screenshot of http://localhost:9999/hello/sayhello*:

![http://localhost:9999/hello/sayhello](img/sayhello.png)

*Screenshot of http://localhost:9999/hello/querybook.html*:

![http://localhost:9999/hello/querybook.html](img/query.png)

*Screenshot of results from http://localhost:9999/hello/querybook.html*:

![Results of http://localhost:9999/hello/querybook.html](img/query_re.png)

*Screenshot of running http://localhost:9999/lis4368/*:

![Tomcat Installation Screenshot](img/Tomcat_lis4368_a2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Santirod05/lis4368/src/f2b8b3e1e11ecbd0df83a81507c825473307462d/a2/README.md "Bitbucket Station Locations")


