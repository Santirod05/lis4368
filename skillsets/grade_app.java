import java.util.Scanner;
import java.text.DecimalFormat;

public class grade_app
{

   public static void main(String[] args) 

   {      
        // Display Operational Messages
       System.out.println("PLease enter grades that range from 0 to 100");
       System.out.println("Grade average and total is rounded to 2 decimal places");
       System.out.println("Note: Program does *not* check for non-numeric characters");
       System.out.println("To end program enter '-1'");
       System.out.println(); // Print blank line 

       // Initialize variables and create scanner obejct 

       double gradeTotal = 0.0;
       int gradeCount = 0;
       double testGrade = 0.0;
			 Scanner sc = new Scanner(System.in);

       while (testGrade != -1)
       {

        // get user input
        System.out.println("Enter grade: ");
        testGrade = sc.nextDouble();

        //accumulate grade count and grade total
        if (testGrade <= 100 && testGrade >= 0)
          {
            gradeCount = gradeCount + 1; 
            gradeTotal = gradeTotal + testGrade;
          }

        else if (testGrade != -1)
          System.out.println("Invalid entry, not counted.");
       }

       double averageGrade = 0;

       // display grade count , total, and average 

       if (gradeTotal == 0)
          {
            averageGrade = 0;
          }

        else 
        {
          averageGrade = gradeTotal / gradeCount; 

        }

        DecimalFormat f = new DecimalFormat ("##.00");

        // System.out.printlm (f.format(d));
      
 	    String message = "\n" + 
      "Grade Count: " + gradeCount + "\n"
      + "Grade Total: " + f.format(gradeTotal) + "\n"
      + "average Grade: " + f.format(averageGrade) + "\n";

      System.out.println(message);
          }
 }
