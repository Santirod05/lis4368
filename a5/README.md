> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Rafael Rodriguez

### Assignment #5 Requirements:

*Sub-Heading:*

1. Include server-side validation from A4
2. Compile serverlet files
 

#### README.md file should include the following items:

* Screenshots of pre-, post-valid user form entry
* Screenshots of MySQL customer table entry


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Pre-Validation*:

![Pre-Validation Screenshot](img/pv.png)

*Screenshot of Post-Validation*:

![Post-Validation Screenshot](img/ppv.png)

*Screenshot of MYSQL Entry*:

![Mysql Entry Screenshot](img/entry.png)

*Screenshot of running JDK SS13*:

![SS13 Screenshot](img/ss13.png)

*Screenshot of running JDK SS14*:

![SS14 Screenshot](img/ss14.png)

*Screenshot of running JDK SS15*:

![SS15 Screenshot](img/ss15.png)



