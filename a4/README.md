> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Rafael Rodriguez

### Assignment #4 Requirements:

*Sub-Heading:*

1. Implement server side data validation for user entered data
2. Include error page if submitted form is empty
3. Java Skillset 10 Count Characters
4. Java Skillset 11 Write/Read count words
5. Java Skillset 12 ASCII App

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Assignment 4 Notes:

* Model View Controller (MVC)is a framework that divides an application's implementation into three discrete component roles: models, views, and controllers.
* Moreover, in computer scienceand information technology, separation of concerns (SoC) is a design principle for separating a computer program into distinct sections(or layers), with each section addressing a separate concern: e.g., presentation layer, business logic layer, data access layer, and persistence layer.
*Model:-The “business” layer. how do we represent the data? Defines business objects, and provides methods for business processing (e.g., Customerand CustomerDB classes).
* Controller: Directs the application “flow”—using servlets. Generally, reads parameters sent from requests, updates the model, saves data to the data store, and forwards updated model to JSPs for presentation (e.g., CustomerServlet).
* View: How do I render the result?Defines the presentation—using .html or .jsp files (e.g., index.html, index.jsp, customerform.jsp, thanks.jsp).

#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/f_valid.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/p_valid.png)

*Screenshot of Skillset 10 Count Characters*:

![Skillset 10 Screenshot](img/ss10.png)

*Screenshot of Skillset 11 Write/Read/Count words*:

![Skillset 11 Screenshot](img/ss11.png)

*Screenshot of Skillset 12 ASCII App *:

![Skillset 12 Screenshot](img/ss12.png)![Skillset 12 Screenshot](img/ss12_1.png)![Skillset 12 Screenshot](img/ss12_2.png)
