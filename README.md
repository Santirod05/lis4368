> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

#  LIS4368 - Advanced Web Applications Development 

## Rafael Rodriguez

### Assignment Requirements:

*Assignment Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL Workbench
    - Write a "Hello-world" Java Servlet
    - Write a Database Servlet Deploying Servlet using @WebServlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Entity Relationship Diagram (ERD)
    - Include data (at least 10 records per table)
    - Provide Bitbucket read-only acess to repo (Language SQL) *must* include README.md. using Markdown syntax, and include links to all of the following files (From README.md):
        - Docs folder: a3.mwb and a3.sql
        - Img folder: a03.png (export a03.mwb file as a3.png)
        - README.md (*MUST* display a3.png ERD)
    - Canvas links: Bitbucket repo

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Implement server side data validation for user entered data
    - Include error page if submitted form is empty
    - Include passed validation
    - Java Skillset 10 Count Characters
    - Java Skillset 11 Write/Read count words
    - Java Skillset 12 ASCII App

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Include server-side validation from A4
    - Compile servlet files
    - Screenshots of pre-, post-valid user form entry
    - Screenshots of MySQL customer table entry

### Project Requirements:

*Project Links:*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Suitably modify meta tags in index.jsp.
    - Change title, navigation links, and header tags appropriately.
    - Add form controls to match attributes of customer entity.
    - Use regex to only allow appropriate characters for each control.

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Complete the JSP/Servlets web application using the MVC framework using basic client/server-side validation.
    - provide : 
        - Create
        - Read 
        - Update
        - and Delete (CRUD) functionality
    - Display customer data for P2

