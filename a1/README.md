> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Rafael Rodriguez

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket.
2. Java/JSP/Servlet Development Installation.
3. Chapter Questions (Chapters 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello:
* Screenshot of running: http://localhost:9999/
* Git commands with short descriptions:
* Bitbucket repo links: https://bitbucket.org/Santirod05/lis4368/src/master/
* Link to local lis4368 web app: http://localhost:9999/lis4368/ 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![Tomcat Installation Screenshot](img/Tomcat.png)

*Screenshot of running http://localhost:9999/lis4368/*:

![Tomcat Installation Screenshot](img/Tomcat_lis4368.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Santirod05/LIS4368/ "Bitbucket Station Locations")
