-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema rsr19a
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `rsr19a` ;

-- -----------------------------------------------------
-- Schema rsr19a
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rsr19a` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `rsr19a` ;

-- -----------------------------------------------------
-- Table `rsr19a`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rsr19a`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `rsr19a`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `rsr19a`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rsr19a`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `rsr19a`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_first` VARCHAR(15) NOT NULL,
  `cus_last` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` VARCHAR(2) NOT NULL,
  `cus_zip` INT(9) ZEROFILL UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `rsr19a`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rsr19a`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `rsr19a`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `rsr19a`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `rsr19a`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `rsr19a`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `rsr19a`;
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetSmart', '368 Hickory Ave.', 'Mcallen', 'TX', 78501, 6754637892, 'petsmart@gmail.com', 'PetsmartTX.com', 450000, 'notes 1');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Petsco', '751 Lois Lane', 'Phoenix', 'AZ', 31705, 6548675897, 'petscoAZ@live.com', 'petscolive.com', 500000, 'Notes 2');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'El Tigre Salvaje ', '637 Berkshire St.', 'Lewis Center', 'OH', 43025, 5467890654, 'Eltigre@gmail.com', 'Tigresalvaje.net', 1000000, 'Notes 3');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Palace', '7835 South Ocean Street ', 'El patron', 'GA', 30736, 7863467589, 'PP@yahoo.com', 'PetPalaca.com', 56748, 'Notes 4');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends ', '9337 Miles Ave.', 'Nashau', 'NH', 03060, 5463797483, 'RAWR@gmail.com', 'furrrrrrrr.com', 787865, 'Notes 5');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Supermarket', '760 Division Circle ', 'Flint', 'MI', 48504, 7583987869, 'PetSupa@hotmail.com', 'supasupamarket.com', 98278974, 'Notes 6');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'La granja', '8032 NE. Prairie Court', 'West Lafayette', 'IN', 47906, 4567886578, 'Lagranja@hotmail.com', 'dagranja.com', 42445, 'Notes 7');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'DDk Exotic', '92 Green Lake St. ', 'Cape Coral', 'FL', 33904, 8905858969, 'ddkexotics@gmail.com', 'ddk.com', 7857854, 'Notes 8');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'El pulgero', '67 Arch Rd. ', 'Largo, FL 33771', 'FL', 33771, 8769406859, 'elpulgue@hotmail.com', 'elpulgue.com', 9828654, 'Notes 9');
INSERT INTO `rsr19a`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'The farm', '8173 Bowman Street', 'Twin Falls, ID 83301', 'ID', 83301, 7849378950, 'Theoneandonlyfarm@yahoo.com', 'thefarm.com', 244542, 'Notes 10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `rsr19a`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `rsr19a`;
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Raymond', 'Cano', '63178 Northshore Dr.', 'Laramie', 'WY', 45761, 6758793465, 'barnett@gmail.com', 53.00, 564.00, 'Notes1');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Laura', 'Ortiz', '7831 Tempest Ln.', 'Strugis', 'SD', 43102, 6784356788, 'ducasse@verizon.net', 56.00, 7653.00, 'Notes2');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Natally', 'Avila', '8624 Baldwin Dr.', 'Panama City', 'FL', 33567, 9546753455, 'aracne@sbcglobal.net\n', 76.00, 6465.00, 'Notes3');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Santiago', 'Rodriguez', '19567 Rutherford Cir.', 'Charleston', 'WV', 65478, 9876453123, 'fallorn@mac.com', 675.00, 960.00, 'Notes4');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Gabriel', 'Roques', '502 Shady Dr.', 'Minneanopolis ', 'MN', 54367, 8764356789, 'majordock@me.com', 22.00, 535.00, 'Notes5');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Steven', 'Rodriguez', '250 N Biscayne Blvd.', 'Miami', 'FL', 33134, 7860945678, 'kourai@yahoo.com', 356.00, 4567.00, 'Notes6');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Toni', 'Lopez', '8075 Nw 7th St', 'Miami', 'Fl', 33172, 3057683456, 'thowell@verizon.net', 67.00, 556.00, 'Notes7');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Andrea', 'Bravo', '86 Van Dyke Ave.', 'De Pere', 'WI', 54115, 7684393758, 'satishr@verizon.net\n', 3.00, 67.00, 'Notes8');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Angie', 'Amaya', '7241 Maiden Street ', 'Sugar Land', 'TX', 77478, 8967589087, 'ylchang@live.com\n', 356.00, 235.00, 'Notes9');
INSERT INTO `rsr19a`.`customer` (`cus_id`, `cus_first`, `cus_last`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Natalie', 'Samara', '553 Ann St. ', 'New Milford', 'CT', 06776, 78693756489, 'gospodin@aol.com', 56.00, 1200.00, 'Notes10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `rsr19a`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `rsr19a`;
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 10, 'Blue Nose Pitbull', 'M', 1200, 2000, 2, 'Grey', '2020/01/17', 'N', 'N', '2 MONTHS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 2, 'SIlver Back Gorilla', 'F', 1000, 1500, 6, 'Black', '2007/08/01', 'Y', 'N', '6 YEARS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 7, 'African Grey Parrot', 'M', 1156, 1300, 4, 'grey / blue ', '2020/01/05', 'Y', 'N', '4 YEARS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 8, 'King Cobra', 'M', 1000, 1500, 3, 'Black / Red', '2010/06/05', 'Y', 'Y', '3 MONTHS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, NULL, 'Italian Mastif ', 'M', 8000, 9000, 1, 'Black', '2020/02/03', 'N', 'N', '1 MONTH OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 7, 'Labrador', 'F', 500, 1200, 3, 'Blonde', '2021/05/20', 'Y', 'N', '3 YEARS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, NULL, 'American Bulldog', 'F', 3000, 3500, 1, 'Black / White', '2019/03/05', 'Y', 'N', '1 YEAR OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, NULL, 'Leaopard Gecko', 'M', 50, 100, 5, 'Leopard', '2020/04/20', 'N', 'N', '5 MONTHS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 2, 'Chimpanzee', 'M', 8000, 9000, 9, 'Black', '2019/04/27', 'N', 'N', '9 MONTHS OLD');
INSERT INTO `rsr19a`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 6, 'Turtle', 'F', 80, 100, 2, 'Red / Orange', '2020/12/20', 'N', 'N', '2 YEARS OLD');

COMMIT;

