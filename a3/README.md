> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Rafael Rodriguez

### Assignment 3 Requirements:

*Deliverables:*
*Three Parts:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 recprds each table)
3. Provide Bitbucket read-only access to repo (Language SQL) *must* include README.md. using Markdown syntax, and include links to all of the following files (From README.md):
    - docs folder: a3.mwb and a3.sql
    - img folder: a3.png (export a3.mwb file as a3.png)
    - README.md (*MUST* display a3.png ERD)
4. Canvas Links: Bitbucket repo

#### README.md file should include the following items:

- Screenshot of A3 ERD that links to the image:

#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:

![A3 ERD](img/a3_erd.png "ERD based upon A3 Requirements")

*Screenshot ERD Pet Store tables*:

![A3 /index.jsp](img/pst.png "A3 ERD Pet Store tables screenshot")

*Screenshot ERD Customer tables*:

![A3 /index.jsp](img/cus.png "A3 ERD Customer Store tables screenshot")

*Screenshot ERD Pet tables*:

![A3 /index.jsp](img/pet.png "A3 ERD Pet tables screenshot")

*Screenshot A3 index.jsp*:

![A3 /index.jsp](img/a3_index.png "A3 Index.jsp screenshot")


*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")