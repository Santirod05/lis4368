> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Development

## Rafael Rodriguez

### Project #2 Requirements:

*Sub-Heading:*

1. Complete the JSP/Servlets web application using the MVC framework.
2. Provide : 
        - Create
        - Read 
        - Update
        - and Delete (CRUD) functionality
3. Display customer data for P2

#### README.md file should include the following items:

* Valid User Form Entry (customerform.jsp)
* Passed Validation (thanks.jsp)
* Display Data (customers.jsp)
* Modify Form (modify.jsp)
* Modified Data (customers.jsp)
* Delete Warning (customers.jsp)
* Associated Database Changes (Select, Insert, Update, Delete)


> This is a blockquote.


#### Assignment Screenshots:

*Screenshot of Valid User Form Entry (customerform.jsp)*:

![Valid User Form Entry Screenshot](img/cf.png)

*Screenshot of Passed Validation (thanks.jsp)*:

![Passed Validation Screenshot](img/pv.png)

*Screenshot of Display Data (customers.jsp)*:

![Display Data Screenshot](img/dd.png)

*Screenshot of Modify Form (modify.jsp)*:

![Modify Form Screenshot](img/mf.png)

*Screenshot of Modified Data (customers.jsp)*:

![Modified Data Screenshot](img/md.png) 

*Screenshot of Delete Warning (customers.jsp)*:

![Delete Warning Screenshot](img/dw.png)

*Screenshot of Associated Database Changes*:

![(Select, Insert, Update, Delete) Screenshot](img/adc1.png)

*Screenshot of Added New Customer Database Changes*:

![(Select, Insert, Update, Delete) Screenshot](img/adc2.png)

*Screenshot of Associated Edited Database Changes*:

![(Select, Insert, Update, Delete) Screenshot](img/adc3.png)

*Screenshot of Associated Deleted Database Changes*:

![(Select, Insert, Update, Delete) Screenshot](img/adc4.png)

